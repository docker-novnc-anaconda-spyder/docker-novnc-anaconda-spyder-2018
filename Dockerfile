FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

############ core novnc install ###############
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    locales-all \
    wget \
    bzip2 \
    ca-certificates \
    apt-utils

RUN apt-get update   &&  apt-get dist-upgrade -y

RUN apt-get install -y --force-yes --no-install-recommends \
        python-numpy \
        software-properties-common \
        wget \
        curl \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        x11vnc \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        libpci-dev \
        xserver-xorg-video-dummy \
        libglu1 \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

ADD startup.sh /
ADD cleanup-cruft.sh /
ADD initialize.sh /

ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
ADD noVNC /noVNC/
ADD noVNC_cert /noVNC_cert/
# make sure the noVNC cert files are  only readable by root
RUN  chmod 400 /noVNC_cert/self.*

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf

############ end core novnc install ###############



############ begin helpful packages ###############
RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 zip \
 unzip \
 git \
 texlive-latex-base \
 texlive-latex-extra \
 texlive-fonts-extra \
 texlive-fonts-recommended \
 texlive-generic-recommended \
 libxrender1 \
 inkscape \
 zip \
 unzip \
 git \
 python3-pyqt5 \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*
 
############ end helpful packages ###############

############ begin spyder #############
# Install conda as ubuntu
RUN mkdir /opt/conda && \
    chown ubuntu:ubuntu /opt/conda 
USER ubuntu
# Configure environment
ENV CONDA_DIR /opt/conda
ENV PATH $CONDA_DIR/bin:$PATH
ENV SHELL /bin/bash
ENV NB_USER ubuntu
ENV NB_UID 1000
ENV HOME /home/$NB_USER
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN cd /tmp && \
    wget --quiet https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh && \
    /bin/bash Anaconda3-2021.05-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
    rm Anaconda3-2021.05-Linux-x86_64.sh  

RUN $CONDA_DIR/bin/conda config --system --prepend channels conda-forge && \
    $CONDA_DIR/bin/conda config --system --set auto_update_conda false && \
    $CONDA_DIR/bin/conda config --system --set show_channel_urls true && \
    conda update --yes anaconda

RUN conda install --yes spyder=5.0.5
 
RUN rm -rf /home/$NB_USER/.cache/yarn 

############ end spyder ###############

USER root

# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6

ENTRYPOINT ["/startup.sh"]
